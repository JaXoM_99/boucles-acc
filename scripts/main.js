var map, chart, clusterGroup;
var coops = [];
var volunteers = [];
var estimationPos = {};

var coopRadius = 2000; //m

$().ready(init);

function init() {
    //caption init
    setupCaption();
    setupCsvImporter();

    const mapbox = (map) => {
        return L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', { foo: 'bar', attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>' }).addTo(map);
    };

    map = L.map('map', { doubleClickZoom: false }).setView([48.1113387, -1.6800198], 12);
    mapbox(map);
    map.on('click', onMapClick);
    map.on('dblclick', onMapDblClick);

    //import database
    displayContentOnMap();

}

function onMapClick(e) {
    // console.log(`{\nlat: ${e.latlng.lat},\nlng:${e.latlng.lng},\nname: "xxxxxxxxxxxxx",\ntype: "xxxxxxxxxxxx"\n},`);
}

function onMapDblClick(e) {
    if (estimationPos.center) {
        estimationPos.center.setLatLng(e.latlng);
        estimationPos.circle.setLatLng(e.latlng);
    } else {
        estimationPos.center = L.marker(e.latlng).addTo(map);
        estimationPos.circle = L.circle(e.latlng, { radius: coopRadius }).addTo(map);
        estimationPos.circleDisplayed = true;
        estimationPos.center.on('click', function (e) {
            if (estimationPos.circleDisplayed) {
                estimationPos.circle.removeFrom(map);
            } else {
                estimationPos.circle.addTo(map);
            }
            estimationPos.circleDisplayed = !estimationPos.circleDisplayed;
        });
    }
    var neighbors = {};
    neighbors.volunteers = db_volunteers.filter(volunteer => map.distance(e.latlng, { lat: volunteer.lat, lng: volunteer.lng }) <= coopRadius);
    neighbors.coops = coops.filter(coop => map.distance(e.latlng, coop.center) <= coopRadius);

    displayNeighbors(neighbors);
}

function coopToExpand(id) {
    if (!coops[id].isExpanded)
        coops[id].expand(map);
}

function coopToRetract(id) {
    if (coops[id].isExpanded)
        coops[id].retract(map);
}

function displayContentOnMap() {
    if (clusterGroup)
        map.removeLayer(clusterGroup);
    clusterGroup = L.markerClusterGroup();
    var id = 0;
    db_localCenters.forEach(center => {
        var coop = new Coop(id++, center.users.filter(user => user.type == "solar-plant"),
            center.users.filter(user => user.type == "house"),
            center.users.filter(user => user.type == "company"),
            center.manager
        );
        coop.display(map);
        coops.push(coop);
    });

    db_volunteers.forEach(volunteer => {
        var marker = L.marker({ lat: volunteer.lat, lng: volunteer.lng });
        var icon;
        switch (volunteer.type) {
            case "house":
                icon = icon_volunteer_house;
                break;
            case "company":
                icon = icon_volunteer_company;
                break;
            case "solar-plant":
                icon = icon_volunteer_solarPlant;
                break;
        }
        if (icon)
            marker.setIcon(icon);
        marker.bindPopup('<h3>' + volunteer.name + '</h3>');
        clusterGroup.addLayer(marker)
        // marker.addTo(map);
    });
    map.addLayer(clusterGroup);
}

function displayNeighbors(neighbors) {
    var domContainer = document.getElementById("neighbors");
    domContainer.innerHTML = "";
    if (neighbors.coops.length > 0 || neighbors.volunteers.length > 0) {
        domContainer.innerHTML += '<hr>';
    }
    if (neighbors.coops.length > 0) {
        domContainer.innerHTML += '<h2>Coopératives à portée</h2>';
        neighbors.coops.forEach(coop => {
            domContainer.innerHTML += '<div class="alert alert-success"><strong>Responsable: ' + coop.manager + '</strong><span class="badge badge-secondary" style="float:right;">contacter</span></div>';
        });
    }
    if (neighbors.volunteers.length > 0) {
        domContainer.innerHTML += '<h2>Volontaires à portée</h2>';
        neighbors.volunteers.forEach(volunteer => {
            domContainer.innerHTML += '<div class="alert alert-success"><strong>' + volunteer.name + '</strong><span class="badge badge-secondary" style="float:right;">contacter</span></div>';
        });
    }

}

function setupCaption() {
    var captionItems = document.querySelectorAll(".caption-item");
    document.getElementById("caption").innerHTML = '<p class="text-info">' + document.querySelector(".caption-item-selected").getAttribute("caption") + '</p>';

    function resetCaption() {
        for (let index = 0; index < captionItems.length; index++) {
            captionItems[index].classList.remove("caption-item-selected");
        }
    }

    for (let index = 0; index < captionItems.length; index++) {
        captionItems[index].addEventListener("click", function (event) {
            resetCaption();
            this.classList.add("caption-item-selected");
            document.getElementById("caption").innerHTML = '<p class="text-info">' + this.getAttribute("caption") + '</p>';
        });

    }
}

function setupCsvImporter() {
    var fileInput = document.getElementById("file-input");
    var fileReader = new FileReader();

    fileReader.addEventListener("loadstart", function () {
        //
    });

    fileReader.addEventListener("load", function () {
        try {
            var fileContent = $.csv.toArrays(fileReader.result, { separator: ";" });
            fileContent.shift();
            fileContent.forEach(row => {
                var name = row[0];
                var loop = row[1];
                var lat = parseFloat(row[2]);
                var lng = parseFloat(row[3]);
                var inputAddress = row[4];
                var outputAddress = row[5];
                if (loop == "") {
                    var volunteer = {};
                    volunteer.type = "house";
                    volunteer.name = name;
                    volunteer.lat = lat;
                    volunteer.lng = lng;
                    volunteer.inputAddress = inputAddress;
                    volunteer.outputAddress = outputAddress;
                    db_volunteers.push(volunteer);
                } else {
                    var user = {};
                    user.type = "house";
                    user.lat = lat;
                    user.lng = lng;
                    user.name = name;
                    user.inputAddress = inputAddress;
                    user.outputAddress = outputAddress;

                    var centers = db_localCenters.filter(center => center.manager == loop);
                    if (centers.length > 0) {
                        console.log("already exists");
                        var center = centers[0];
                        center.users.push(user);
                    } else {
                        console.log("doesn't exist yet");
                        var center = {};
                        center.manager = loop;
                        center.users = [user];
                        db_localCenters.push(center);
                    }
                }
            });
            displayContentOnMap();
        } catch (error) {
            alert(error);
        }
    });

    fileInput.addEventListener("change", function () {
        fileReader.readAsText(this.files[0]);
    });
}