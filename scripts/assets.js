// assets
//TODO: faire des cadres avec un fond orange
var icon_solarPlant = L.icon({
    iconUrl: "assets/solar-panel-in.png",
    iconSize: [40, 40]
});
var icon_house = L.icon({
    iconUrl: "assets/house-in.png",
    iconSize: [40, 40]
});
var icon_company = L.icon({
    iconUrl: "assets/company-in.png",
    iconSize: [40, 40]
});
//TODO: faire un cadre avec un fond blanc
var icon_coop = L.icon({
    iconUrl: "assets/coop.png",
    iconSize: [60, 60]
});

//TODO: faire des cadres avec un fond vert
var icon_volunteer_solarPlant = L.icon({
    iconUrl: "assets/solar-panel-vol.png",
    iconSize: [40, 40]
});
var icon_volunteer_house = L.icon({
    iconUrl: "assets/house-vol.png",
    iconSize: [40, 40]
});
var icon_volunteer_company = L.icon({
    iconUrl: "assets/company-vol.png",
    iconSize: [40, 40]
});