class Coop {
    constructor(id, solarPlants, houses, companies, manager) {
        this._isExpanded = false;
        this._links = [];

        var centerLat = 0;
        var centerLng = 0;

        this._manager = manager;

        this._solarPlants = solarPlants;
        this._solarPlantMarkers = [];
        solarPlants.forEach(solarPlant => {
            centerLat += solarPlant.lat;
            centerLng += solarPlant.lng;
            var marker = L.marker({ lat: solarPlant.lat, lng: solarPlant.lng });
            marker.id = id;
            marker.on('dblclick', function (e) {
                coopToRetract(this.id);
            });
            marker.setIcon(icon_solarPlant);
            marker.bindPopup('<h3>' + solarPlant.name + '</h3><p>membre de la boucle "' + this._manager + '"</p>');
            this._solarPlantMarkers.push(marker);
        });

        this._houses = houses;
        this._houseMarkers = [];
        houses.forEach(house => {
            centerLat += house.lat;
            centerLng += house.lng;
            var marker = L.marker({ lat: house.lat, lng: house.lng });
            marker.id = id;
            marker.on('dblclick', function (e) {
                coopToRetract(this.id);
            });
            marker.setIcon(icon_house);
            marker.bindPopup('<h3>' + house.name + '</h3><p>membre de la boucle "' + this._manager + '"</p>');
            this._houseMarkers.push(marker);
        });

        this._companies = companies;
        this._companyMarkers = [];
        companies.forEach(company => {
            centerLat += company.lat;
            centerLng += company.lng;
            var marker = L.marker({ lat: company.lat, lng: company.lng });
            marker.id = id;
            marker.on('dblclick', function (e) {
                coopToRetract(this.id);
            });
            marker.setIcon(icon_company);
            marker.bindPopup('<h3>' + company.name + '</h3><p>membre de la boucle "' + this._manager + '"</p>');
            this._companyMarkers.push(marker);
        });

        centerLat /= (solarPlants.length + houses.length + companies.length);
        centerLng /= (solarPlants.length + houses.length + companies.length);

        this._center = L.marker({ lat: centerLat, lng: centerLng });
        this._center.setIcon(icon_coop);
        this._center.bindPopup(this._getPopup());
        this._center.id = id;

        this._center.on('dblclick', function (e) {
            coopToExpand(this.id);
        });
    }

    display(map) {
        //draw center
        this._center.addTo(map);
    }

    expand(map) {
        this._isExpanded = true;
        //hide center
        this._center.removeFrom(map);

        //draw links
        this._solarPlants.forEach(plant => {
            this._houses.forEach(house => {
                var link = new L.Polyline([{ lat: plant.lat, lng: plant.lng }, { lat: house.lat, lng: house.lng }]);
                link.addTo(map);
                this._links.push(link);
            });
            this._companies.forEach(company => {
                var link = new L.Polyline([{ lat: plant.lat, lng: plant.lng }, { lat: company.lat, lng: company.lng }]);
                link.addTo(map);
                this._links.push(link);
            });
        });

        //draw markers
        this._solarPlantMarkers.forEach(marker => {
            marker.addTo(map);
        });
        this._houseMarkers.forEach(marker => {
            marker.addTo(map);
        });
        this._companyMarkers.forEach(marker => {
            marker.addTo(map);
        });
    }

    retract(map) {
        //hide elts
        this._isExpanded = false;
        this._links.forEach(elt => {
            elt.removeFrom(map);
        });
        this._houseMarkers.forEach(elt => {
            elt.removeFrom(map);
        });
        this._companyMarkers.forEach(elt => {
            elt.removeFrom(map);
        });
        this._solarPlantMarkers.forEach(elt => {
            elt.removeFrom(map);
        });

        //display center
        this._center.addTo(map);
    }

    _getPopup() {
        var userNbre = this._companies.length + this._solarPlants.length + this._houses.length;
        var content = "";
        content += '<h3>' + this._manager + '</h3>';
        content += '<ul>';
        if (this._solarPlants.length > 0) {
            content += '<li>' + this._solarPlants.length + ' centrale' + (this._solarPlants.length > 1 ? 's' : '') + '</li>';
        }
        if (this._houses.length > 0) {
            content += '<li>' + this._houses.length + ' habitation' + (this._houses.length > 1 ? 's' : '') + '</li>';
        }
        if (this._companies.length > 0) {
            content += '<li>' + this._companies.length + ' commerce' + (this._companies.length > 1 ? 's' : '') + '</li>';
        }
        content += '</ul>';
        return content;
    }

    get isExpanded() {
        return this._isExpanded;
    }

    get center() {
        return this._center.getLatLng();
    }

    get manager() {
        return this._manager;
    }
}